.. mandyoc-docs documentation master file, created by
   sphinx-quickstart on Mon Nov  2 14:45:10 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

MANDYOC Documentation
=====================

Welcome to the MANDYOC Documentation page. 

MANDYOC is short for MANtle DYnamics simulatOr Code. It is a numerical code written on top of the `PETSc`_ library and it simulates thermochemical convection of the Earth's mantle.

On this page, you will be able to find relevant information on how the code works, how it was implemented, how to install it, the description of the parameters and input files, some applications and some useful examples.

.. toctree::
    :numbered: 
    :maxdepth: 2
    :caption: Getting started

    files/theory
    files/implementation
    files/installation
    files/parameter-file
    files/input
    files/benchmarks
    files/references



Need any help?
--------------

If you need any help, send us an email to sacek@usp.br or jamison.assuncao@usp.br

.. _PETSc: https://www.mcs.anl.gov/petsc/


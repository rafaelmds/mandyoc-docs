How to install
==============

MANDYOC installation is very simple and it consists of installing both `PETSc`_ and MANDYOC from each one's Bitbucket repository. 

.. warning:: 
	The following installation steps work for both Linux and macOS machines **only** and no tests were made to install MANDYOC on Windows machines yet.

PETSc Installation
------------------

MANDYOC requires the `PETSc`_ library to run. The first step is to clone the repository into your machine or download if from the `PETSc repository`_::
	
	git clone https://bitbucket.org/petsc/petsc/src/maint/

Once it is cloned, you can use the terminal of your machine to navigate to its directory and run the command below. Be sure to change the values of ``PETSC_DIR`` and the ``PETSC_ARCH`` to the proper path to the `PETSc`_ directory. 

.. literalinclude:: src/configure.txt
   :language: bash
   :linenos:

Alternatively, you can copy the command to `file.sh` and run it inside the `PETSc`_ directory using `sh`::
	
	# in path/to/petsc/directory
	sh file.sh

.. note::
	The ``.\configure`` command installs PETSc with debugging mode on (*-\-with-debugging=1*). This is always recommended when running MANDYOC for the first time but it decreases its performance and it is not recommended for time consuming simulations. Once you have PETSc and MANDYOC working properly, consider installing PETSc with debugging mode off (*-\-with-debugging=0*).

Follow the instructions that pop up on the terminal. For further information about the PETSc library, check the `PETSc website`_.

MANDYOC Installation
--------------------

To install the MANDYOC you need to clone it to a local repository in your machine. Navigate to the directory you wish to install MANDYOC and type the command below or download the code from the `MANDYOC repository page`_.

.. code-block:: bash

   git clone https://bitbucket.org/victorsacek/mandyoc/src/master/

Next, navigate until the ``mandyoc/src/`` folder and edit the *makefile* adding the ``PETSC_DIR`` and the ``PETSC_ARCH`` that was used during the PETSc installation. Once the *makefile* is properly edited and saved, run on the terminal::

	make all


.. _PETSc: https://www.mcs.anl.gov/petsc/
.. _PETSc website: https://www.mcs.anl.gov/petsc/
.. _PETSc repository: https://bitbucket.org/petsc/petsc/src/maint/
.. _MANDYOC repository page: https://bitbucket.org/victorsacek/mandyoc/src/master/